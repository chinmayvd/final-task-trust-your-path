#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include "pid.h"

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <eve_msgs/Control.h>

#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH
#define PI 3.14159265

Steering::Steering():private_n("~")
{
  this->steering = 0;
  this->throttle = 0;
  this->brake = 0;
  this->gear = 2;

  this->waypoint_x = 0;
  this->waypoint_y = 0;

  private_n.param("Kp", Kp, 0.0);
  private_n.param("Ki", Ki, 0.0);
  private_n.param("Kd", Kd, 0.0);

  private_n.param("sKp", sKp, 0.0);
  private_n.param("sKi", sKi, 0.0);
  private_n.param("sKd", sKd, 0.0);


  publisher = n.advertise<eve_msgs::Control>("eve_drive", 1000);

};

void Steering::publishCommand()
{
  eve_msgs::Control msg;
  msg.throttle = this->throttle;
  msg.brake = this->brake;
  msg.steer = this->steering;
  msg.shift_gears = this->gear;
  publisher.publish(msg);
  ROS_INFO("OK");
};

/*void Steering::openBag()
{

};*/

void Steering::callback(const nav_msgs::OdometryConstPtr& inp) //Callback
{
//  for(int i = 0; i<10; i++)
  //{
    current_x = inp->pose.pose.position.x;
    current_y = inp->pose.pose.position.y;
     //Car vector
    double car_vec_x = current_x - prev_x;
    double car_vec_y = current_y - prev_y;

    //Waypoint vector
    double path_vec_x = waypoint_x - prev_waypoint_x;
    double path_vec_y = waypoint_y - prev_waypoint_y;

    //Vector between the current Waypoint and current car position
    double vec_x = waypoint_x - current_x;
    double vec_y = waypoint_y - current_y;

    std::cout << "VEC" << vec_x<< std::endl;
    std::cout << "Car_vec" << car_vec_x << std::endl;
    std::cout << "Path" << path_vec_x << std::endl;
    //Error terms
    double throttle_err = hypot(current_x - waypoint_x, current_y - waypoint_y);
    double steer_err = acos( (vec_x*car_vec_x + vec_y*car_vec_y)/(hypot(vec_x, vec_y)*hypot(car_vec_x, car_vec_y)) )*180/PI;

    this->throttle = PIDcalc(throttle_err, Kp, Ki, Kd, -1, 1);
    this->steering = PIDcalc(steer_err, sKp, sKi, sKd, -12, 12);
    this->gear = 2;

    if(this->throttle <= 0)
    {
      this->brake = -1*this->throttle;
      this->throttle = 0;
    }
    else
    {
      this->brake = 0;
    }

    ROS_INFO("Throtte: %lf", this->throttle);
    ROS_INFO("Steering: %lf", this->steering);

    prev_waypoint_x = waypoint_x;
    prev_waypoint_y = waypoint_y;

    prev_x = current_x;
    prev_y = current_y;

    this->publishCommand();
  //}
};

double Steering::PIDcalc(double error, double Kp, double Ki, double Kd, double min_limit, double max_limit) // Calculates the Values to be published
{
  std::cout << "Error" << error << std::endl;

  d_error = error - prev_error;
  i_error += error;

  output = Kp*error + Ki*i_error + Kd*d_error;

  if(output >= max_limit)
    output = max_limit;
  else if(output <= min_limit)
    output = min_limit;

  prev_error = error;
  return output;
}



void Steering::run()
{
  subscriber = n.subscribe("odom", 1000, &Steering::callback, this);
  rosbag::Bag bag;
  bag.open("/home/chinmay/final_task_ws/src/drive_path/test.bag", rosbag::bagmode::Read);

  std::vector<std::string> topics;
  topics.push_back(std::string("/odom"));

  rosbag::View view(bag, rosbag::TopicQuery(topics));

  ros::Rate loop_rate(50);
  foreach(rosbag::MessageInstance const m, view)
  {
    nav_msgs::Odometry::ConstPtr msg = m.instantiate<nav_msgs::Odometry>();
    if(msg != NULL)
    {
      waypoint_x = msg->pose.pose.position.x;
      waypoint_y = msg->pose.pose.position.y;

      ros::spinOnce();
      loop_rate.sleep();
      ROS_INFO("waypoint_x: %lf", waypoint_x);
    }
  }
  bag.close();
};





int main(int argc, char **argv)
{
  ros::init(argc, argv, "Steering");

  Steering tracker;
  tracker.run();
  return 0;
}
