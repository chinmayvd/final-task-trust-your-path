#ifndef __PID_H_INCLUDED__
#define __PID_H_INCLUDED__


#include <iostream>
#include <math.h>
#include <string>
#include <vector>

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <eve_msgs/Control.h>

#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH
#define PI 3.14159265

class Steering
{
  public:
    ros::NodeHandle n;
    ros::NodeHandle private_n;

    ros::Publisher publisher;
    ros::Subscriber subscriber;

    //Throttle Parameters
    double Kp;
    double Ki;
    double Kd;

    //Steering Parameters
    double sKp;
    double sKi;
    double sKd;

    //eve_msgs
    double steering;
    double throttle;
    double brake;
    double gear;

    //Current co-ordinates of the car
    double current_x;
    double current_y;

    //Previous co-ordinates of the car
    double prev_x = 0.0001;
    double prev_y = 0.0001;

    //Current Waypoint
    double waypoint_x;
    double waypoint_y;

    //Previous Waypoints
    double prev_waypoint_x = 0.0001;
    double prev_waypoint_y = 0.0001;

    //pid calculation
    double i_error = 0;
    double d_error = 0;
    double output = 0;
    double prev_error = 0;

    Steering(); // constructer

    void openBag();
    void callback(const nav_msgs::OdometryConstPtr& inp);
    double PIDcalc(double error, double Kp, double Ki, double Kd, double min_limit, double max_limit);
    void publishCommand();
    void run();

};

#endif
